import httpClient from './httpClient';

const END_POINT = 'occurence.json';


const getAllOccurence = async () => {
    const response = await httpClient.get(END_POINT, {params:{language:"auto"}});
    return response
}

// you can pass arguments to use as request parameters/data
// const getOccurence = (user_id) => httpClient.get(END_POINT, { user_id });
// maybe more than one..
// const createUser = (username, password) => httpClient.post(END_POINT, { username, password });

export {
    getAllOccurence,
    
}